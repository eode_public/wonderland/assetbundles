**This is the home page of [this documentation](http://wonderland.eode.studio/docs/assetbundles-_introduction.html)**

## Introduction

### Version
> Released, Deprecated

> About AssetBundles: "you should use the Addressable Assets package instead, as Unity has deprecated the AssetBundle Manager."

### Dependencies
- `Common`

## Scope
AssetBundles tackles the issue of working with asset bundles in Unity which can prove to be quite cumbersome, by leveraging the straightforwardness of async/await based code made available with the new scripting upgrade to [.NET 4.6](https://docs.unity3d.com/Manual/ScriptingRuntimeUpgrade.html).

This plugin is only focused on using the asset bundles, to build and visualize them, we advise you to use the [Asset Bundle Browser](https://assetstore.unity.com/packages/tools/utilities/asset-bundle-browser-93571).

### Capabilities
* Simple code using [Async/Await](https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2013/hh191443(v=vs.120))
* Local or remote bundles location
* Automatic bundles loading / unloading
* Automatic dependencies management
* Simulation mode
* Patching system

## Example
```C#
async void Start () {
	// Init with a source url
	await AssetBundles.Init(Application.streamingAssetsPath);

	// Load one asset (Cube from wonderland-tests)
	var myCubePrefab = await AssetBundles.LoadAsset<GameObject>("wonderland-tests", "Cube");

	// Load multiple assets (all prefabs in wonderland-tests)
	var allMyPrefabs = await AssetBundles.LoadAllAssets<GameObject>("wonderland-tests");
}
```
